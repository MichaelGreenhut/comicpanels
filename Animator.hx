package urgame;

/**
 * ...
 * @author Michael Greenhut
 */
import flambe.Entity;
import flambe.System;
import flambe.display.Sprite;
import haxe.xml.Fast;
import flambe.asset.File;
import flambe.display.ImageSprite;
import flambe.asset.AssetPack;
import flambe.swf.MovieSprite;
import flambe.swf.MovieSymbol;
import flambe.swf.Format;
import flambe.swf.Library;
import flambe.Component;


class Animator extends Listener
{
	private var _images:Array<Dynamic>;
	private var _timeFrames:Array<Dynamic>;
	//private var _bubbles:Map<String,BubbleMaker>;
	private var _imageLoadingQueue:Array<Dynamic>;
	private var _currentName:String;
	private var _loadedImageSprites:Map<String,Sprite>;
	private var _artistMode:Bool;
	private var _animatorReady:Bool = false;
	private var _xmlFileName:String;
	private var _imagePath:String;
	private var _paused:Bool;
	private var _autoBegin:Bool;
	private var _firstSprite:Sprite;
	private var _currentImageName:String;
	private var _animationQueues:Array<AnimationQueue>;
	//private var _soundHandler:SoundHandler;
	private var _soundPath:String;
	private var _flumpMovieHandler:FlumpMovieHandler;
	
	private var _assetPack:AssetPack;
	private var _fast:Fast;
	//private var _parentEntities:Array<Entity>;

	public function new(pack:AssetPack, data:File, autoBegin:Bool = true) 
	{
		super();
		//_parentEntities = new Array<Entity>();
		_flumpMovieHandler = new FlumpMovieHandler();
		_animationQueues = new Array<AnimationQueue>();
		_assetPack = pack;
		var xml:Xml = Xml.parse(data.toString());
		//trace(xml.toString());
		_fast = new Fast(xml);
		_autoBegin = autoBegin;
		init();
		//System.root.addChild(new Entity().add(this));
		EventHub.getInstance().addListener("waitForAction", this, false);
		
	}
	
	public override function listen(event:String, sender:Dynamic = null):Void 
	{
		if (event == "imageDone")
		{
			trace("image done");
			animate();
		}
	}
	
	public function flumpMovieHandler():FlumpMovieHandler
	{
		return _flumpMovieHandler;
	}
	
	public function isAnimatorReady():Bool
	{
		return _animatorReady;
	}
	
	public function firstSprite():Sprite
	{
		return _firstSprite;
	}
	
	public function move(xPos:Float, yPos:Float):Void 
	{
		_firstSprite.x.animateTo(xPos,0.1);
		_firstSprite.y.animateTo(yPos, 0.1);	
	}
	
	private function init() 
	{
		_timeFrames = new Array<Dynamic>();
		_loadedImageSprites = new Map<String,Sprite>();
		_imageLoadingQueue = new Array<Dynamic>();
		begin();
	}
	
	public function isPaused():Bool
	{
		return _paused;
	}
	
	public function pause():Void 
	{
		if (!_paused)
		{
			_paused = true;
		}
		else 
		{
			_paused = false;
		}
	}
	
	private function begin():Void 
	{
		//_images = new Array<Dynamic>();

		for (timeFrame in _fast.nodes.timeFrame) 
		{
			_images = new Array<Dynamic>();
			
			for (image in timeFrame.nodes.image)
			{
				var iName:String = image.att.name+";";
				if (image.has.folder)
					iName += image.att.folder;
				else if (image.has.file) 
					iName += image.att.file;
				//trace("pushing " + iName);
				_imageLoadingQueue.push(iName);
				
				var imageData = new Map<String,Dynamic>();
				imageData.set("name", iName);

				imageData.set("startX", Std.parseFloat(image.att.startX));
				imageData.set("startY", Std.parseFloat(image.att.startY));
				if (image.has.startWidth)
					imageData.set("startWidth", Std.parseFloat(image.att.startWidth));
				if (image.has.startHeight)
					imageData.set("startHeight", Std.parseFloat(image.att.startHeight));
				if (image.has.startAlpha)
					imageData.set("startAlpha", Std.parseFloat(image.att.startAlpha));
				if (image.has.textFont)
					imageData.set("textFont", image.att.textFont);
				if (image.has.fontSize)
					imageData.set("fontSize", Std.parseFloat(image.att.fontSize));
				
				imageData.set("steps", new Array<Dynamic>());
				for (step in image.nodes.step)
				{
					
					var stepData = new Map<String,Dynamic>();
					if (step.hasNode.x)
						stepData.set("x", Std.parseFloat(step.node.x.innerHTML));
					if (step.hasNode.y)
						stepData.set("y", Std.parseFloat(step.node.y.innerHTML));
					if (step.hasNode.width)
						stepData.set("width", Std.parseFloat(step.node.width.innerHTML));
					if (step.hasNode.height)
						stepData.set("height", Std.parseFloat(step.node.height.innerHTML));
					if (step.hasNode.alpha)
						stepData.set("alpha", Std.parseFloat(step.node.alpha.innerHTML));
					if (step.hasNode.rotation)
						stepData.set("rotation", Std.parseFloat(step.node.rotation.innerHTML)); 
					if (step.hasNode.scaleX)
						stepData.set("scaleX", Std.parseFloat(step.node.scaleX.innerHTML));
					if (step.hasNode.scaleY)
						stepData.set("scaleY", Std.parseFloat(step.node.scaleY.innerHTML)); 
					if (step.hasNode.sound)
					{
						if (step.node.sound.has.loops)
							stepData.set("soundLoops", Std.parseInt(step.node.sound.att.loops));
						else 
							stepData.set("soundLoops", 1);
						stepData.set("sound", step.node.sound.innerHTML);
					}
					if (step.hasNode.loop)
						stepData.set("loop", step.node.loop.innerHTML);
					if (step.hasNode.ease)
						stepData.set("ease", step.node.ease.innerHTML);
					if (step.hasNode.cueAction)
						stepData.set("cueAction", step.node.cueAction.innerHTML);
					if (step.hasNode.waitForAction)
						stepData.set("waitForAction", step.node.waitForAction.innerHTML);
						
					if (step.hasNode.text) //---> CURRENT way of handling text bubbles
					{
						if (step.node.text.has.color)
							stepData.set("color", Std.parseInt(step.node.text.att.color));
						else 
							stepData.set("color", 0x000000);
						if (step.node.text.has.fontSize)
							stepData.set("fontSize", Std.parseFloat(step.node.text.att.fontSize));
						else if (image.has.fontSize)
						{
								stepData.set("fontSize", imageData.get("fontSize"));
						}
						else 
							stepData.set("fontSize", 12);
							
						if (step.node.text.has.textFont)
							stepData.set("textFont", step.node.text.att.textFont);
						else if (image.has.textFont)
						{
								stepData.set("textFont", imageData.get("textFont"));
						}
						else 
							stepData.set("textFont", "TIMES.TTF");
						if (step.node.text.has.remainFor)
							stepData.set("remainFor", Std.parseFloat(step.node.text.att.remainFor))
						else 
							stepData.set("remainFor", 2);
							
							
						stepData.set("text", step.node.text.innerHTML);
						stepData.set("textX", Std.parseFloat(step.node.text.att.x));
						stepData.set("textY", Std.parseFloat(step.node.text.att.y));
						}
						if (step.hasNode.time)
						{
							stepData.set("time", Std.parseFloat(step.node.time.innerHTML));
						}
						if (step.hasNode.delay)
							stepData.set("delay", Std.parseFloat(step.node.delay.innerHTML));
						imageData.get("steps").push(stepData);
						
					}
					_images.push(imageData);
				}
	
			_timeFrames.push(_images.concat([]));
		}
		
		loadImages();
	}
	
	private function waitForAction():Void 
	{
		EventHub.getInstance().fire("waitForAction");
	}
	
	private function loadImages():Void 
	{
		if (_imageLoadingQueue.length > 0)
		{
			_currentName = _imageLoadingQueue.shift();
			var nameArray:Array<String> = _currentName.split(";");
			//trace ("name array len " + nameArray.length);
			//trace ("name array" + nameArray[0].length);
			if (nameArray[1].length > 0)
			{
				if (nameArray[1].indexOf(".xml") > -1)
				{
					var conts = _assetPack.getFile(nameArray[1]);
					//handling child Animators here

					var animator:Animator = new Animator(_assetPack,conts,true);
					//_parentEntities.push(new Entity().add(animator.firstSprite()));
					_loadedImageSprites.set(_currentName, animator.firstSprite());
					
				}
				else 
				{
					var library:Library = new Library(_assetPack, nameArray[1]);
					var movie:MovieSprite = library.createMovie(nameArray[0]);
					movie.position = 0;
					//movie.paused = true;
				//	movie.loop = false;
					_flumpMovieHandler.storeMovie(movie);
				
					_loadedImageSprites.set(_currentName, movie);
				}
			}
			else 
			{
				var imageSprite = new ImageSprite(_assetPack.getTexture(nameArray[0]));
				_loadedImageSprites.set(_currentName, imageSprite);
			}
			
			
			loadImages();
		}
		else 
		{
			if (_autoBegin)
				animate();
		}
	}
	
	private function animate():Void 
	{
		//trace("timeframes " + _timeFrames.length);
		if (_timeFrames.length > 0)
		{
			var images = _timeFrames.shift();
			
			
			for (i in 0...images.length)
			{
				var steps:Array<Dynamic> = images[i].get("steps");
				var startX = images[i].get("startX");
				var startY = images[i].get("startY");
				var startWidth:Null<Int> = images[i].get("startWidth");
				var startHeight:Null<Int> = images[i].get("startHeight");
				var startAlpha:Null<Int> = images[i].get("startAlpha");
				var imageName = images[i].get("name");
				var textFont:Null<String> = images[i].get("textFont");
				var fontSize:Null<Float> = images[i].get("fontSize");

				var sprite = _loadedImageSprites.get(imageName);
				trace(sprite);
				sprite.x._ = startX;
				sprite.y._ = startY;
				var textSprites = new Map<String,Dynamic>();
				
				if (startAlpha != null)
					sprite.alpha._ = startAlpha;
			
				//trace("currewnt sprite is " + _currentSprite);
				var entity:Entity = new Entity();
				
				
				if (_firstSprite == null) 
				{
					_firstSprite = new Sprite();
					System.root.addChild(new Entity().add(_firstSprite), false);
					//trace("adding sprite" + imageName);
				}
				_firstSprite.owner.addChild(entity);
				if (sprite.owner == null)
					entity.add(sprite);
					
				var aq:AnimationQueue = new AnimationQueue();
				if (i == images.length - 1)
					EventHub.getInstance().addListener("imageDone", this);
					//aq.addEventListener(AnimationQueue.IMAGE_DONE, animate);
				_currentImageName = imageName;
			
				_animationQueues.push(aq);
				
				aq.init(sprite, steps, textSprites, entity);
			}
		}
	}
	
}