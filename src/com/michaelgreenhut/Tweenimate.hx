package com.michaelgreenhut ;
import flambe.animation.AnimatedFloat;
import flambe.animation.Ease;
import flambe.display.Sprite;
import haxe.Timer;

/**
 * ...
 * @author Michael Greenhut
 */
class Tweenimate
{
	private static var _mainTimer:Timer;
	private static var _reverseTimer:Timer;
	
	public function new() 
	{
		
	}
	
	public static function to(object:Sprite, seconds:Float, tweenProps:Dynamic, delay:Float = 0, onComplete:Void->Void = null, ?easing:EaseFunction, reverse:Bool = false)
	{
		//if (_mainTimer != null)
			//_mainTimer.stop();
		//else 
		if (_reverseTimer != null)
			_reverseTimer.stop();
		_mainTimer = Timer.delay(function() {
		var originalProps:Dynamic = {};
		var fields = Reflect.fields(tweenProps);
		if (easing == null)
			easing = Ease.quadInOut;
		for( param in fields)
		{
		
			var property:AnimatedFloat = cast(Reflect.getProperty(object, param), AnimatedFloat);
			//trace(property.animateTo, param, Reflect.field(tweenProps,param), seconds, easing);
			if (property != null)
			{
				Reflect.setField(originalProps, param, property._);
				property.animateTo(Reflect.field(tweenProps, param), seconds, easing);
			}
		}
		if (reverse)
		{
			_reverseTimer = Timer.delay(function() { to(object, seconds, originalProps, 0, onComplete, easing, false); },Std.int(seconds*1000));
		}
		else 
		{
			if (onComplete != null)
				Timer.delay(onComplete, Std.int(seconds * 1000));
			
		}
		
		},Std.int(delay * 1000));
	}

	
}