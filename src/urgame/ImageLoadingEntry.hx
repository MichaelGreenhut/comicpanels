package urgame;

/**
 * ...
 * @author Michael Greenhut
 */
class ImageLoadingEntry
{
	public static var BASIC:String = "basic";
	public static var FLUMP:String = "flump";
	public static var ANIMATOR:String = "animator";
	public var name:String;
	public var file:String;
	public var folder:String;
	public var type:String;

	public function new() 
	{
		type = BASIC;
	}
	
	public function toString():String 
	{
		return name;
	}
	
}