package urgame;
import flambe.util.SignalConnection;
import flambe.util.Signal2;

import flambe.util.*;


/**
 * ...
 * @author Michael Greenhut
 */
class EventHub
{
	private static var _eventHub:EventHub;
	
	private var _listeners:Array<Dynamic>;

	public function new(ehmaker:EHMaker) 
	{
		_listeners = new Array<Dynamic>();
	}
	
	public function addListener(type:String, listener:Listener, removeWhenDone:Bool = true, allowSender:Bool = false):Void 
	{
		
		var s2:Signal2<String,Dynamic> = new Signal2<String,Dynamic>();
		var sc:SignalConnection;
		if (removeWhenDone)
		{
			
			sc = s2.connect(listener.listen).once();
		}
		else
		{
			
			sc = s2.connect(listener.listen);
		}
		//var sc:SignalConnection = new SignalConnection(s0, listener);
		
		_listeners.push( { listenerType:type, signal:s2, remove:removeWhenDone, connection:sc, listenerTarget:listener } );//listenerTarget:listener, remove:removeWhenDone});
	}
	
	public function removeListener(type:String, listener:Listener):Void 
	{
		var ll:Int = _listeners.length-1;
		for (i in -ll...1) 
		{
			if (Reflect.field(_listeners[-i], "listenerType") == type && listener == Reflect.field(_listeners[-i], "listenerTarget")) 
			{
				_listeners[-i].connection.dispose();
				_listeners.splice(-i, 1);
			}
		}
	}
	
	public function wipeListeners(exception:Listener = null):Void 
	{
		var ll = _listeners.length - 1;
		//trace("wiping ", ll);
		for (i in -ll...1) 
		{
			//trace("eye ", i);
			if (Reflect.field(_listeners[-i],"listenerTarget") != null)
				if (_listeners[-i].listenerTarget == exception)
					continue;
			/*8if (Type.getClass(Reflect.field(_listeners[-i], "listenerTarget")) == urgame.Animator)
			{
				var doomedMovie:Animator = cast(Reflect.field(_listeners[ -i], "listenerTarget"), urgame.Animator);
				if (doomedMovie != null)
					doomedMovie.dispose();
			}*/
			var sc:SignalConnection = cast(Reflect.field(_listeners[-i], "connection"), SignalConnection);
			if (sc != null)
				sc.dispose();
			_listeners.splice(-i , 1);
		}
	}
	
	public function fire(type:String, data:Dynamic = null):Void 
	{
		for (i in 0..._listeners.length) 
		{
			if (Reflect.field(_listeners[i], "listenerType") == type)
			{
				var signal:Signal2<String,Dynamic> = _listeners[i].signal;
				signal.emit(type,data);
				//var listener:Listener = _listeners[i].listenerTarget;//cast(Listener,Reflect.field(_listeners[i],"listenerTarget"))
				//listener.listen(type);
				if (Reflect.field(_listeners[i],"remove")) 
				{
					_listeners[i].garbageCollect = true;
				}
			}
		}
		
		for (i in _listeners.length...0)
		{
			if (_listeners[i].garbageCollect)
				_listeners.splice(i, 1);
		}
		
	}
	
	public static function getInstance():EventHub
	{
		if (_eventHub == null) 
			_eventHub = new EventHub(new EHMaker());
			
		return _eventHub;
	}
	
}

private class EHMaker 
{
	public function new() 
	{
		
	}
	
}