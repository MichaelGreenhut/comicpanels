package urgame;

/**
 * ...
 * @author Michael Greenhut
 */
import flambe.display.Font;
import flambe.display.Texture;
import flambe.Entity;
import flambe.System;
import flambe.display.Sprite;
import flash.errors.Error;
import format.tools.Image;
import haxe.xml.Fast;
import flambe.asset.File;
import flambe.display.ImageSprite;
import flambe.asset.AssetPack;
import flambe.swf.MovieSprite;
import flambe.swf.MovieSymbol;
import flambe.swf.Format;
import flambe.swf.Library;
import flambe.asset.Manifest;
import flambe.Component;
import flambe.util.Signal0;


class Animator extends Listener
{
	public var animationDone:Signal0;
	private var _images:Array<Dynamic>;
	private var _timeFrames:Array<Dynamic>;
	private var _imageLoadingQueue:Array<ImageLoadingEntry>;
	private var _currentEntry:ImageLoadingEntry;
	private var _loadedImageSprites:Map<String,Sprite>;
	private var _animatorReady:Bool = false;
	private var _autoBegin:Bool;
	private var _firstSprite:Sprite;
	private var _flumpMovieHandler:FlumpMovieHandler;
	private var _currentPack:AssetPack;
	private var _keyCode:Int;
	private var _startingAnimation:Signal0;

	public function new(pack:AssetPack, data:File, autoBegin:Bool = true, movieHandler:FlumpMovieHandler = null, isChild:Bool = false) 
	{
		animationDone = new Signal0();
		_currentPack = pack;
		_startingAnimation = new Signal0();
		_keyCode = Std.random(10000);
		super();
		this.owner = new Entity();
		this.owner.add(this);
		//_parentEntities = new Array<Entity>();
		if (movieHandler == null)
			_flumpMovieHandler = new FlumpMovieHandler();
		else 
			_flumpMovieHandler = movieHandler;
		//_animationQueues = new Array<AnimationQueue>();
		
		var xml:Xml = Xml.parse(data.toString());
		_autoBegin = autoBegin;
		init(data.toString());
		//System.root.addChild(new Entity().add(this));
		EventHub.getInstance().addListener("waitForAction", this, false);
		EventHub.getInstance().addListener("nextTimeFrame", this, false);
		EventHub.getInstance().addListener("kickoff", this, false);
		EventHub.getInstance().addListener("end", this, true);
	}
	
	public function getAssetPack():AssetPack
	{
		return _currentPack;
	}
	
	public function keyCode():Int 
	{
		return _keyCode;
	}
	
	public override function listen(event:String, sender:Dynamic = null):Void 
	{
		if (event == "nextTimeFrame")
		{
			if (sender == _keyCode)
			{
				//trace("timeFrame done");
				animate();
			}
		}
		else 
		if (event == "kickoff")
		{
			if (sender == _firstSprite)
			{
				animate();
				_animatorReady = true;
				_startingAnimation.emit();
			}
		}
		else 
		if (event == "end")
		{
			animationDone.emit();
		}
	}
	
	public function flumpMovieHandler():FlumpMovieHandler
	{
		return _flumpMovieHandler;
	}
	
	public function isAnimatorReady():Bool
	{
		return _animatorReady;
	}
	
	public function firstSprite():Sprite
	{
		return _firstSprite;
	}
	
	public function move(xPos:Float, yPos:Float):Void 
	{
		_firstSprite.x.animateTo(xPos,0.1);
		_firstSprite.y.animateTo(yPos, 0.1);	
	}
	
	private function init(xmldata:String) 
	{
		_timeFrames = new Array<Dynamic>();
		_loadedImageSprites = new Map<String,Sprite>();
		_imageLoadingQueue = new Array<ImageLoadingEntry>();
		begin(xmldata);
	}
	
	public function startingAnimation():Signal0
	{
		return _startingAnimation;
	}
	
	private function begin(xmldata:String):Void 
	{
		var xmlString = Xml.parse(xmldata);
		var fast:Fast = new Fast(xmlString);
		//_images = new Array<Dynamic>();
		for (timeFrame in fast.nodes.timeFrame) 
		{
			_images = new Array<Dynamic>();
			
			for (image in timeFrame.nodes.image)
			{
				var imageLoadingEntry:ImageLoadingEntry = new ImageLoadingEntry();
				var imageData = new Map<String,Dynamic>();
				//trace("image " + image.att.name, _keyCode);
				imageLoadingEntry.name = image.att.name;
				var iName:String = image.att.name+";";
				if (image.has.folder)
				{
					iName += image.att.folder;
					imageLoadingEntry.folder = image.att.folder;
					imageLoadingEntry.type = ImageLoadingEntry.FLUMP;
				}
				else if (image.has.file) 
				{
					iName += image.att.file;
					imageLoadingEntry.file = image.att.file;
					imageLoadingEntry.type = ImageLoadingEntry.ANIMATOR;
				}
				//trace("pushing " + iName);
				var imageQueueString:String = _imageLoadingQueue.join("*");
				if (imageQueueString.indexOf(image.att.name) == -1)
					_imageLoadingQueue.push(imageLoadingEntry);
				//imageData.set("name", iName);
				imageData.set("name", image.att.name);
				imageData.set("startX", Std.parseFloat(image.att.startX));
				imageData.set("startY", Std.parseFloat(image.att.startY));
				if (image.has.startWidth)
					imageData.set("startWidth", Std.parseFloat(image.att.startWidth));
				if (image.has.startHeight)
					imageData.set("startHeight", Std.parseFloat(image.att.startHeight));
				if (image.has.startAlpha)
					imageData.set("startAlpha", Std.parseFloat(image.att.startAlpha));
				if (image.has.textFont)
					imageData.set("textFont", image.att.textFont);
				if (image.has.fontSize)
					imageData.set("fontSize", Std.parseFloat(image.att.fontSize));
				
				imageData.set("steps", new Array<Dynamic>());
				for (step in image.nodes.step)
				{
					var stepData = new Map<String,Dynamic>();
					if (step.hasNode.end)
						stepData.set("end", "true");
					if (step.hasNode.nextTimeFrame)
						stepData.set("nextTimeFrame", "true");
					if (step.hasNode.x)
						stepData.set("x", Std.parseFloat(step.node.x.innerHTML));
					if (step.hasNode.y)
						stepData.set("y", Std.parseFloat(step.node.y.innerHTML));
					if (step.hasNode.width)
						stepData.set("width", Std.parseFloat(step.node.width.innerHTML));
					if (step.hasNode.height)
						stepData.set("height", Std.parseFloat(step.node.height.innerHTML));
					if (step.hasNode.alpha)
						stepData.set("alpha", Std.parseFloat(step.node.alpha.innerHTML));
					if (step.hasNode.rotation)
						stepData.set("rotation", Std.parseFloat(step.node.rotation.innerHTML)); 
					if (step.hasNode.scaleX)
						stepData.set("scaleX", Std.parseFloat(step.node.scaleX.innerHTML));
					if (step.hasNode.scaleY)
						stepData.set("scaleY", Std.parseFloat(step.node.scaleY.innerHTML)); 
					if (step.hasNode.sound)
					{
						if (step.node.sound.has.loops)
							stepData.set("soundLoops", Std.parseInt(step.node.sound.att.loops));
						else 
							stepData.set("soundLoops", 1);
						
						if (_currentPack.getSound(step.node.sound.innerHTML) != null)
						{
							stepData.set("sound", _currentPack.getSound(step.node.sound.innerHTML));
							
						}
					}
					if (step.hasNode.loop)
						stepData.set("loop", step.node.loop.innerHTML);
					if (step.hasNode.ease)
						stepData.set("ease", step.node.ease.innerHTML);
					if (step.hasNode.cueAction)
						stepData.set("cueAction", step.node.cueAction.innerHTML);
					if (step.hasNode.waitForAction)
						stepData.set("waitForAction", step.node.waitForAction.innerHTML);
					if (step.hasNode.waitForChoice)
						stepData.set("waitForChoice", step.node.waitForChoice.innerHTML);
					if (step.hasNode.choiceTimer)
						stepData.set("choiceTimer", step.node.choiceTimer.innerHTML);
						
					if (step.hasNode.text) //---> CURRENT way of handling text bubbles
					{
						if (step.node.text.has.preserveAspectRatio)
							stepData.set("preserveAspectRatio", step.node.text.att.preserveAspectRatio);
						if (step.node.text.has.color)
							stepData.set("color", Std.parseInt(step.node.text.att.color));
						else 
							stepData.set("color", 0x000000);
						if (step.node.text.has.fontSize)
							stepData.set("fontSize", Std.parseFloat(step.node.text.att.fontSize));
						else if (image.has.fontSize)
						{
								stepData.set("fontSize", imageData.get("fontSize"));
						}
						else 
							stepData.set("fontSize", 12);
							
						if (step.node.text.has.textFont)
						{
							var fontName:String = step.node.text.att.textFont.split(".")[0];
							trace(fontName);
						//	var font:Font = new Font(_currentPack, fontName);
							stepData.set("textFont", fontName);
						}
						else if (image.has.textFont)
						{
							var fontName:String = image.att.textFont.split(".")[0];
							stepData.set("textFont", fontName);
						}
						else 
							stepData.set("textFont", "Arial");
						if (step.node.text.has.background)
						{
							stepData.set("textBackground", step.node.text.att.background);
						}
						if (step.node.text.has.bubbleShape)
						{
							stepData.set("textBubbleShape", step.node.text.att.bubbleShape);
						}
						if (step.node.text.has.tail)
						{
							stepData.set("textTail", step.node.text.att.tail);
						}
						if (step.node.text.has.remainFor)
							stepData.set("remainFor", Std.parseFloat(step.node.text.att.remainFor))
						else 
							stepData.set("remainFor", 2);
								
						stepData.set("text", step.node.text.innerHTML);
						stepData.set("textX", Std.parseFloat(step.node.text.att.x));
						stepData.set("textY", Std.parseFloat(step.node.text.att.y));
						}
						if (step.hasNode.time)
						{
							stepData.set("time", Std.parseFloat(step.node.time.innerHTML));
						}
						if (step.hasNode.delay)
							stepData.set("delay", Std.parseFloat(step.node.delay.innerHTML));
						imageData.get("steps").push(stepData);
						
					}
					_images.push(imageData);
				}
	
			_timeFrames.push(_images.concat([]));
		}
		fast = null;
		loadImages();
	}
	
	private function waitForAction():Void 
	{
		EventHub.getInstance().fire("waitForAction");
	}
	
	private function loadImages():Void 
	{
		if (_imageLoadingQueue.length > 0)
		{
			_currentEntry = _imageLoadingQueue.shift();
			
			if (_loadedImageSprites.get(_currentEntry.name) != null)
			{
				loadImages();
				return;
			}
			
			if (_currentEntry.type != ImageLoadingEntry.BASIC) //(nameArray[0].indexOf(".") == -1)  //if it's not a simple image file.  If it is, nameArray[0] contains the entire path and thus the texture, and we skip to the 'else' part.  
			{
				if (_currentPack != null)
				{
					var location:String = _currentEntry.folder;//folderArray.join("/"); //issue 2
					trace("location", location);
					if (_currentEntry.type == ImageLoadingEntry.ANIMATOR) //if it's another animator class
					{
						var conts = _currentPack.getFile(location);
						//handling child Animators here
						var animator:Animator = new Animator(_currentPack,conts,false,_flumpMovieHandler, true);
						_loadedImageSprites.set(_currentEntry.name, animator.firstSprite());
					}
					else if (_currentEntry.type == ImageLoadingEntry.FLUMP)
					{
						var library:Library = new Library(_currentPack, location);
						var movie:MovieSprite = library.createMovie(_currentEntry.name);
						_flumpMovieHandler.storeMovie(movie);
						_loadedImageSprites.set(_currentEntry.name, movie);
					}
					else throw new Error("image type" + _currentEntry.type + "not recognized!");
				}
			}
			else
			{
				var nameSansExtension = _currentEntry.name.split(".")[0];
				var imageSprite = new ImageSprite(_currentPack.getTexture(nameSansExtension));
				_loadedImageSprites.set(_currentEntry.name, imageSprite);   //TODO: What about checking for non-simple 
																		//image sprite duplicates?
			}
			
			loadImages();
		}
		else 
		{
			_firstSprite = new Sprite();
			this.owner.addChild(new Entity().add(_firstSprite));
			if (_autoBegin)
			{
				animate();
				_animatorReady = true;
				_startingAnimation.emit();
			}
		}
	}
	
	private function animate():Void 
	{
		if (_timeFrames.length > 0)
		{
			var images = _timeFrames.shift();

			for (i in 0...images.length)
			{
				var steps:Array<Dynamic> = images[i].get("steps");
				var startX = images[i].get("startX");
				var startY = images[i].get("startY");
				var startWidth:Null<Int> = images[i].get("startWidth");
				var startHeight:Null<Int> = images[i].get("startHeight");
				var startAlpha:Null<Int> = images[i].get("startAlpha");
				var imageName = images[i].get("name");
				for(i in 0...steps.length)
				{
					if (steps[i].get("textFont") != null)
						steps[i].set("textFont", new Font(_currentPack, steps[i].get("textFont")));
					if (steps[i].get("textBackground") != null)
						steps[i].set("textBackground", _currentPack.getTexture(steps[i].get("textBackground").split(".")[0]));
					if (steps[i].get("textBubbleShape") != null)
						steps[i].set("textBubbleShape", _currentPack.getTexture(steps[i].get("textBubbleShape").split(".")[0]));
					if (steps[i].get("textTail") != null)
						steps[i].set("textTail", _currentPack.getTexture(steps[i].get("textTail").split(".")[0]));	
				}
				var fontSize:Null<Float> = images[i].get("fontSize");

				var sprite = _loadedImageSprites.get(imageName);
				trace("getting sprite", sprite, " of image name ", imageName);
				if (imageName.indexOf(".xml") > -1)
				{
					urgame.EventHub.getInstance().fire("kickoff", sprite);
				}
				//trace(sprite);
				sprite.x._ = startX;
				sprite.y._ = startY;
				var textSprites = new Map<String,Dynamic>();
				
				if (startAlpha != null)
					sprite.alpha._ = startAlpha;
			
				var entity:Entity = new Entity();			
				_firstSprite.owner.addChild(entity);
				if (sprite.owner == null)
					entity.add(sprite);
			
				var aq:AnimationQueue = new AnimationQueue(_keyCode);
				
				aq.init(sprite, steps, textSprites, entity);
			}
		}
		else 
			animationDone.emit();
	}
	
	public override function dispose():Void 
	{
		_images = [];
		_timeFrames = [];
		_imageLoadingQueue = [];
		for (key in _loadedImageSprites.keys()) 
		{
			_loadedImageSprites.get(key).dispose();
		}
		if (_firstSprite != null)
			if (_firstSprite.owner != null)
				_firstSprite.owner.dispose();
		//_flumpMovieHandler = null;  //TODO:  Make a proper dispose routine for this
		EventHub.getInstance().removeListener("waitForAction", this);
		EventHub.getInstance().removeListener("nextTimeFrame", this);
		EventHub.getInstance().removeListener("kickoff", this);
		super.dispose();
	}
}