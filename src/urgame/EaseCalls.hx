package urgame;
import flambe.animation.Ease;
/**
 * ...
 * @author Michael Greenhut
 */
class EaseCalls
{

	public function new() 
	{
		
		//funcs();
	}
	
	public function backFuncs():Void 
	{
		Ease.backIn;
		Ease.backOut;
		Ease.backInOut;
	}
	
	public function quadFuncs():Void 
	{
		Ease.quadIn;
		Ease.quadOut;
		Ease.quadInOut;
	}
	public function bounceFuncs():Void 
	{
		Ease.bounceIn;
		Ease.bounceOut;
		Ease.bounceInOut;
	}
	
	public function circFuncs():Void 
	{
		Ease.circIn;
		Ease.circOut;
		Ease.circInOut;
	}
	
	public function cubeFuncs():Void 
	{
		Ease.cubeIn;
		Ease.cubeOut;
		Ease.cubeInOut;
	}
	
	public function expoFuncs():Void 
	{
		Ease.expoIn;
		Ease.expoOut;
		Ease.expoInOut;
	}
	public function quartFuncs():Void 
	{
		Ease.quartIn;
		Ease.quartOut;
		Ease.quartInOut;
	}
	public function quintFuncs():Void 
	{
		Ease.quintIn;
		Ease.quintOut;
		Ease.quintInOut;
	}
	public function sineFuncs():Void 
	{
		Ease.sineIn;
		Ease.sineOut;
		Ease.sineInOut;
	}
	
}