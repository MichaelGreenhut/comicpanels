package urgame;

import flambe.display.ImageSprite;
import flambe.swf.BitmapSymbol;
import flambe.swf.MovieSprite;
import flambe.display.ImageSprite;
import flambe.util.SignalConnection;
import flambe.swf.Library;
import flambe.Entity;
import flambe.math.Rectangle;

/**
 * ...
 * @author Michael Greenhut
 * 
 * TODO:  Lots of obsolete methods to clean up
 */
class FlumpMovieHandler
{

	private var _flumpMovies:Array<MovieSprite>;
	private var _signalConnections:Map<MovieSprite,SignalConnection>;
	private var _currentIndex:Int = 0;
//	private var _id:Float;
	//private var _moviePlayer:MoviePlayer;
	
	public function new() 
	{
	//	_id = Std.random(5000);
		//_moviePlayer = new MoviePlayer();
		_signalConnections = new Map<MovieSprite,SignalConnection>();
		_flumpMovies = new Array<MovieSprite>();
	
	}
	
	public function storeMovie(ms:MovieSprite):Void 
	{
		_flumpMovies.push(ms);
		//trace("speee", ms.speed._);
		//ms.speed._ = 0.5;
		var currentIndex = _currentIndex;
		toggleOuterMovie(ms, false, false);
		toggleInnerMovieSpeed(ms);
		//toggleInnerMovies(ms, true, null, true);
	//	trace("sss" + ms.getLayer("Mask", false));
		var buffer:Float = 2;
		if (ms.getLayer("Mask", false) != null)
		{
			
			var mask:ImageSprite = cast(ms.getLayer("Mask").firstComponent, ImageSprite);
			ms.scissor = new Rectangle( buffer-mask.getNaturalWidth() / 2, buffer-mask.getNaturalHeight() / 2, mask.getNaturalWidth()-buffer, mask.getNaturalHeight()-buffer);
			mask.scissor = new Rectangle(0, 0, 0, 0);
			
		}
		//trace("STORING" + ms.symbol.name + " in " + _id);
		setMovieByIndex(_flumpMovies.length - 1);
		
		//_moviePlayer.
		
	}
	
	public function playCurrentMovie():Void 
	{
		//toggleOuterMovie(_flumpMovies[_currentIndex], true);
		_flumpMovies[_currentIndex].position = 0;
		_flumpMovies[_currentIndex].paused = false;
	}
	
	public function stopCurrentMovie():Void 
	{
		//toggleOuterMovie(_flumpMovies[_currentIndex], false);
		_flumpMovies[_currentIndex].paused = true;
	}
	
	public function stopInnerMovie(ms:MovieSprite, layer:String):Void 
	{
		for (i in 0...ms.symbol.layers.length)
		{
			var layerName = ms.symbol.layers[i].name;
			if (layerName == layer)
			{
				var ms:MovieSprite = cast(ms.getLayer(layerName).firstComponent, MovieSprite);
				ms.paused = true;
				break;
			}
		}
	}
	
	public function toggleInnerMovieSpeed(ms:MovieSprite):Void 
	{
		for (i in 0...ms.symbol.layers.length)
		{
			var layerName = ms.symbol.layers[i].name;
			var layer = ms.symbol.layers[i];
			var frames = layer.frames;
			
			if (Type.getClass(ms.getLayer(layerName).firstComponent) == MovieSprite)
			{
				var ms1:MovieSprite = cast(ms.getLayer(layerName).firstComponent, MovieSprite);
			}
			
		}
	}
	
	public function toggleInnerMovies(ms:MovieSprite, value:Bool, layer:String = null, loop:Bool = false):Void 
	{
		for (i in 0...ms.symbol.layers.length)
		{
			var layerName = ms.symbol.layers[i].name;
			//var layer = ms.symbol.layers[i];
			//var keyFrames = layer.keyframes;
			if ((layer == null) || (layerName == layer))
			{
				if (Type.getClass(ms.getLayer(layerName).firstComponent) == MovieSprite)
				{
					var ms1:MovieSprite = cast(ms.getLayer(layerName).firstComponent, MovieSprite);
					ms1.position = 0;
					ms1.paused = !value;
					//if (value)
						//ms1.speed._ = 0.5;
					toggleLoop(loop, ms1);
				}
			}
		}
	}
	
	public function toggleOuterMovie(ms:MovieSprite, value:Bool, loop:Bool = false):Void 
	{
		ms.position = 0;
		ms.paused = !value;
		toggleLoop(loop,ms);
	}
	
	
	
	public function toggleAllMovies(ms:MovieSprite, value:Bool):Void 
	{ 
		toggleOuterMovie(ms, value);
		toggleInnerMovies(ms, value);
	}
	
	public function setMovieByIndex(index:Int):Void
	{
		_currentIndex = index;
		//return _flumpMovies[index];
	}
	
	//
	//  TODO:  Think of a better search algorithm
	//
	public function setMovieByInstance(mv:MovieSprite):Void 
	{
		for (i in 0..._flumpMovies.length)
		{
			if (_flumpMovies[i] == mv)
			{
				_currentIndex = i;
				break;
			}
		}
	}
	
	public function currentMovie():MovieSprite
	{
		return _flumpMovies[_currentIndex];
	}
	
	public function layer(layerName:String):MovieSprite
	{
		return _flumpMovies[_currentIndex].getLayer(layerName).get(MovieSprite);
	}
	
	public function gotoKeyFramePosition(layerName:String, keyFrame:Int, numKeyFrames:Int):Void 
	{
		var interval:Float = getKeyframeInterval(layerName, numKeyFrames);
		layer(layerName).position = keyFrame * interval;
	}
	
	public function getDuration(layerName:String):Float 
	{
		return layer(layerName).symbol.duration;
	}
	
	private function getKeyframeInterval(layerName:String, numKeyFrames:Int):Float 
	{
		return getDuration(layerName) / numKeyFrames;
	}
	
	public function toggleLoop(value:Bool, movieSprite:MovieSprite):Void 
	{
		//trace("called");
		value = !value;
		if (_signalConnections.get(movieSprite) != null)
			_signalConnections.get(movieSprite).dispose();
			
		if (value == true)
			_signalConnections.set(movieSprite, movieSprite.looped.connect(function() { movieSprite.position = movieSprite.symbol.duration; movieSprite.paused = true; } ));
		else 
			movieSprite.paused = false;
			//_signalConnections.push(movieSprite.looped.connect(function() { movieSprite.paused = value; movieSprite.position = movieSprite.symbol.duration; trace(value, movieSprite.symbol.name); } ).once());
	}
	
	public function remove(ms:MovieSprite):MovieSprite
	{
		var moviesLength:Int = -_flumpMovies.length;
		var mv:MovieSprite = null;
		for (i in moviesLength...1)
		{
			if (_flumpMovies[-i] == ms)
			{
				mv = _flumpMovies[ -i];
				 _flumpMovies.splice(-i, 1);
			}
		}
		
		return mv;
	}
	
	public function clear():Void 
	{
		for (i in 0..._flumpMovies.length) 
		{
			if (_flumpMovies[i] != null)
			{
				_flumpMovies[i].dispose();
			}
		}
		var sk = _signalConnections.keys();
		for (sk in _signalConnections )
		{
			sk.dispose();
		}
		_signalConnections = new Map<MovieSprite,SignalConnection>();
		_flumpMovies = new Array<MovieSprite>();
	}
	
}