package urgame;

import haxe.xml.Fast;
import flambe.Entity;
import flambe.System;
import flambe.asset.AssetPack;
import flambe.asset.File;
import flambe.swf.MovieSprite;
import flambe.swf.MovieSymbol;
import flambe.swf.Format;
import flambe.swf.Library;

/**
 * ...
 * @author Michael Greenhut
 */
class FlumpAnimator extends Listener
{
	
	private var _fast:Fast;
	private var _movieSprites:Array<MovieSprite>;
	private var _movieEntities:Array<Entity>;

	public function new(pack:AssetPack, file:String) 
	{
		super();
		_movieEntities = new Array<Entity>();
		_movieSprites = new Array<MovieSprite>();
		var contents:File = pack.getFile("flump.xml");
		var xml:Xml = Xml.parse(contents.toString());
		_fast = new Fast(xml);
		getMovieSprites(pack);
		
	}
	
	private function getMovieSprites(pack:AssetPack):Void 
	{
		for (movieSprite in _fast.nodes.fla) 
		{
			var folderName:String = movieSprite.att.folder; 
			var topMovieName:String = movieSprite.att.topMovie;
			//trace(folderName);
			//trace(topMovieName);
			var library:Library = new Library(pack, folderName);
			var movie = library.createMovie(topMovieName);
			var entity = new Entity();
			entity.add(movie);
			_movieEntities.push(entity);
			_movieSprites.push(movie);
			System.root.addChild(entity);
			
		}
	}
	
	
	
}