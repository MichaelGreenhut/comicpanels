package urgame;
import flambe.display.BlendMode;
import flambe.display.Font;
import flambe.display.Graphics;
import flambe.display.ImageSprite;
import flambe.display.Sprite;
import flambe.display.TextSprite;
import flambe.display.Texture;
import flambe.Entity;
import flambe.System;
import format.tools.Image;

/**
 * ...
 * @author Michael Greenhut
 */
class TextBubble extends ImageSprite
{
	private var _text:TextSprite;
	private var _bubbleSprite:ImageSprite;
	private var _tailSprite:ImageSprite;
	private var _background:Texture;
	private var _textBuffer:Float = 20;
	private var _preserveAspectRatio:Bool;
	public var wrapWidth:Float = 170;

	public function new(font:Font, text:String, bubbleShape:Texture, tailShape:Texture, background:Texture, preserveAspectRatio:Bool = false) 
	{
		super(null);
		_preserveAspectRatio = preserveAspectRatio;
		this.owner = new Entity();
		this.owner.add(this);
		text = text.split("\\n").join(" \n");
		_text = new TextSprite(font, text);
		if (_text.getNaturalWidth() > wrapWidth)
			_text.setWrapWidth(wrapWidth);
		
		_text.owner = new Entity();
		_text.owner.add(_text);
		var bubbleScales:Array<Float> = scaleToText(bubbleShape, 5, 5);
		
		var bubbleMask:Texture = getMaskedTexture(background, bubbleShape, 0, 0, bubbleScales[0], bubbleScales[1]);
		//trace("bubble mask scale", bubbleMask.width, bubbleMask.height);
		_bubbleSprite = new ImageSprite(bubbleMask);
		
		_bubbleSprite.owner = new Entity();
		_bubbleSprite.owner.add(_bubbleSprite);
		
		if (tailShape != null)
		{
			var tailScales:Array < Float> = scaleToText(tailShape,0,0);
			var tailMask:Texture = getMaskedTexture(background,tailShape, 0,0,1, 1);
			_tailSprite = new ImageSprite(tailMask);
			_tailSprite.owner = new Entity();
			_tailSprite.owner.add(_tailSprite);
			setTailXY();
		}
		
			//scaleToText();
		_text.x._ = ((Sprite.getBounds(_bubbleSprite.owner).width * bubbleScales[0] ) - (Sprite.getBounds(_text.owner).width)) / 2;
		_text.y._ = ((Sprite.getBounds(_bubbleSprite.owner).height * bubbleScales[1] )  - (Sprite.getBounds(_text.owner).height)) / 2;
	
		
		this.owner.addChild(_bubbleSprite.owner);
		this.owner.addChild(_text.owner);
		if (_tailSprite != null)
			this.owner.addChild(_tailSprite.owner);
		trace("scale4",_text.scaleX._, _bubbleSprite.scaleX._,_bubbleSprite.scaleY._);

	}
	
	public function applyOuterScale(newScaleX:Float, newScaleY:Float):Void 
	{
		trace("bub", _bubbleSprite.scaleX._);
		var oldScaleX:Float = _bubbleSprite.scaleX._;
		var oldScaleY:Float = _bubbleSprite.scaleX._;
		_bubbleSprite.scaleX._ *= newScaleX;
		_bubbleSprite.scaleY._ *= newScaleY;
		
		_text.x._ = ((Sprite.getBounds(_bubbleSprite.owner).width * _bubbleSprite.scaleX._) - (_text.getNaturalWidth())) / 2;
		_text.y._ = ((Sprite.getBounds(_bubbleSprite.owner).height * _bubbleSprite.scaleY._)  - (Sprite.getBounds(_text.owner).height)) / 2;
	}
	
	public function orientWithTail(newX:Float, newY:Float, targetX:Float, targetY:Float, targetWidth:Float = 50, targetHeight:Float = 50):Void 
	{
		setXY(newX, newY);
		_tailSprite.setAnchor(0, _tailSprite.getNaturalHeight() / 2);
		trace("target x", targetX, "target y", targetY, "bubble x", newX, "bubble y", newY);
		//trace("bubble width", (Sprite.getBounds(_bubbleSprite.owner).width / 2), "bubble height", (Sprite.getBounds(_bubbleSprite.owner).height / 2));
		//trace("should be:", -1 * (Math.atan2(newY+(Sprite.getBounds(_bubbleSprite.owner).height/2), newX+(Sprite.getBounds(_bubbleSprite.owner).width/2))* (180/Math.PI)));
		//trace((y._+targetY) - y._, newY);
		//trace((x._+targetX) - x._, newX);
		var dy:Float = targetY+(targetWidth/2) - (newY + (Sprite.getBounds(_bubbleSprite.owner).height / 2));
		var dx:Float = targetX+(targetHeight/2) - (newX + (Sprite.getBounds(_bubbleSprite.owner).width/2));
		var angle:Float = Math.atan2(dy, dx);
		//angle += (-90 * (Math.PI / 180));  
		var translatedAngle:Float = (angle * 180 / Math.PI);
		//translatedAngle += -90;//add -90 degrees, since the tail starts pointing straight down)
		trace("full angle ", translatedAngle, x, y);
		_tailSprite.rotation._ = translatedAngle;
		
		trace("SET angle",translatedAngle);
		//normalize();
		
		
	}
	
	private function scaleToText(shapeToScale:Texture, destX:Float = 0, destY:Float = 0):Array<Float>
	{
		var ratio:Float = shapeToScale.width / shapeToScale.height;
		var originalTextWidth:Float = Sprite.getBounds(_text.owner).width;
		var originalTextHeight:Float = Sprite.getBounds(_text.owner).height;
		var scaleX:Float = 1;
		var scaleY:Float = 1;
		if (shapeToScale.width >= shapeToScale.height)
		{
			//trace("scale", (Sprite.getBounds(_text.owner).width + _textBuffer ), Sprite.getBounds(_bubbleSprite.owner).width);
			scaleX = (_text.getNaturalWidth() + destX + _textBuffer*2 )/ (shapeToScale.width);//(_text.getNaturalHeight() * _text.getNaturalWidth()) / (_bubbleSprite.getNaturalHeight() * _bubbleSprite.getNaturalWidth());
			//trace("scale2", _bubbleSprite.scaleX._, (Sprite.getBounds(_text.owner).width + _textBuffer ), Sprite.getBounds(_bubbleSprite.owner).width);
		
			if (_preserveAspectRatio)
				scaleY = scaleX / ratio;
			else 
			    scaleY = (_text.getNaturalHeight() + destY + _textBuffer*2) / (shapeToScale.height);
		}
		else 
		{ 
			scaleY = (_text.getNaturalHeight() + destY + _textBuffer*2) / shapeToScale.height;
			if (_preserveAspectRatio)
				scaleX = scaleY * ratio;
			else 
				scaleX = (_text.getNaturalWidth()+ destX + _textBuffer*2 ) / shapeToScale.width;//(_text.getNaturalHeight() * _text.getNaturalWidth()) / (_bubbleSprite.getNaturalHeight() * _bubbleSprite.getNaturalWidth());
		
				
		}
		trace("returning scales", scaleX, scaleY);
		return [scaleX, scaleY];
	}
	
	private function setTailXY():Void 
	{
		_tailSprite.setXY((Sprite.getBounds(_bubbleSprite.owner).width ) / 2, (Sprite.getBounds(_bubbleSprite.owner).height )/2 - _tailSprite.getNaturalHeight()/2);
		//trace("scale2.5", _bubbleSprite.scaleX._, Sprite.getBounds(_bubbleSprite.owner).width);
		
		//_tailSprite.setScale(Math.max(_bubbleSprite.scaleX._,_bubbleSprite.scaleY._));
		//_text.setScaleXY(1 / Math.abs(_bubbleSprite.scaleX._), 1 / Math.abs(_bubbleSprite.scaleY._));
		//trace("scale3",1 / Math.abs(_bubbleSprite.scaleX._), (Sprite.getBounds(_text.owner).width), Sprite.getBounds(_bubbleSprite.owner).width);

		//_text.x._ -= (Sprite.getBounds(_text.owner).width - originalTextWidth)/2;
		//_text.y._ -= (Sprite.getBounds(_text.owner).height - originalTextHeight) / 2;
		//this.setAnchor(Sprite.getBounds(_bubbleSprite.owner).width / 2, Sprite.getBounds(_bubbleSprite.owner).height / 2);
	}
	
	private function normalize():Void 
	{
		var originalTextWidth:Float = Sprite.getBounds(_text.owner).width;
		var originalTextHeight:Float = Sprite.getBounds(_text.owner).height;
		_bubbleSprite.setScaleXY(1, 1);
		_text.setScaleXY(1, 1);
		_text.x._ -= (Sprite.getBounds(_text.owner).width - originalTextWidth)/2;
		_text.y._ -= (Sprite.getBounds(_text.owner).height - originalTextHeight)/2;
	
		_tailSprite.setScaleXY(1, 1);
	}
	
	public function getMaskedTexture(image:Texture, mask:Texture, destX:Float = 0, destY:Float = 0, scaleX:Float, scaleY:Float):Texture
	{
		
		//mask.graphics.scale(scaleX, scaleY);
		var mask2:Texture = System.renderer.createTexture(mask.width, mask.height);
		mask2.graphics.scale(scaleX, scaleY);
		mask2.graphics.drawTexture(mask, destX, destY);
		var texture:Texture = System.renderer.createTexture(mask.width, mask.height); 
		var graphics:Graphics = texture.graphics;
		graphics.save();
		//graphics.scale(scaleX, scaleY);
		graphics.drawTexture(image, 0, 0);
		graphics.setBlendMode(BlendMode.Mask);
		graphics.drawTexture(mask2, destX, destY);//
		
		//graphics.setAlpha(0.4);
		
		graphics.restore();
		return texture;
	}
	
}