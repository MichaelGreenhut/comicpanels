package urgame;

import com.michaelgreenhut.Tweenimate;
import flambe.asset.AssetPack;
import flambe.display.Font;
import flambe.display.ImageSprite;
import flambe.display.Sprite;
import flambe.display.TextSprite;
import flambe.display.Texture;
import flambe.script.Script;
import flambe.animation.AnimatedFloat;
import flambe.script.AnimateTo;
import flambe.script.Delay;
import flambe.script.Repeat;
import flambe.script.Sequence;
import flambe.script.CallFunction;
import flambe.script.Parallel;
import flambe.script.Action;
import flambe.animation.Ease;
import flambe.sound.Sound;
import flambe.System;
import haxe.Timer;
import urgame.EaseCalls;
import flambe.animation.Sine;
import flambe.Entity;


/**
 * ...
 * @author Michael Greenhut
 */
class AnimationQueue extends Listener
{
	private var _steps:Array<Dynamic>;
	public static var WAIT_FOR_ACTION:String = "waitForAction";
	private var _entity:Entity;
	private var _eases:EaseCalls;
	private var _dataInWaiting:Dynamic;
	private var _keyCode:Int;

	public function new(keyCode:Int) 
	{
		_keyCode = keyCode;
		super();
		_eases = new EaseCalls();
	}
	
	public override function listen(event:String, sender:Dynamic = null):Void 
	{
		if (event == "signalContinue")
		{
			if (_dataInWaiting.pr != null)
			{
				if (_dataInWaiting.text != null)
				{
					Tweenimate.to(_dataInWaiting.text, _dataInWaiting.toT, { alpha:0 } );
					if (_dataInWaiting.pr.alpha == null)
						_dataInWaiting.pr.alpha = 1;
				}
				tween(_dataInWaiting.pr, _dataInWaiting.im, _dataInWaiting.toT, _dataInWaiting.de, _dataInWaiting.ea, _dataInWaiting.params);
			}
			else 
				prepareNextAnimation(_dataInWaiting.im);
		}
	}
	
	public function init(image:Sprite, steps:Array<Dynamic>, textSprites:Map<String,Dynamic> = null, entity:Entity = null):Void 
	{
		_steps = steps;
		_entity = entity;
		//trace("animating...");
		animate(image, false, textSprites);
	}
	
	public function animate(image:Sprite, rev:Bool = false, textSprites:Map<String,Dynamic> = null):Void
	{
		if (_steps.length > 0) 
		{
			var step:Map<String,Dynamic> = _steps.shift();
			var toX:Null<Int> = step.get("x");
			var toY:Null<Int> = step.get("y");
			var toWidth:Null<Int> = step.get("width");
			var toHeight:Null<Int> = step.get("height");
			var toAlpha:Null<Int> = step.get("alpha");
			var toTime:Null<Int> = step.get("time");
			var del:Null<Int> = step.get("delay");
			var rotation:Null<Float> = step.get("rotation");
			var scaleX:Null<Float> = step.get("scaleX");
			var scaleY:Null<Float> = step.get("scaleY");
			var easeName:Null<String> = step.get("ease");
			var nextTimeFrame:Null<String> = step.get("nextTimeFrame");
			var nextTimeFrame:Null<Bool> = step.get("nextTimeFrame");
			var end:Null<String> = step.get("end");
			var preserveAspectRatio:Null<String> = step.get("preserveAspectRatio");
		
			var sound:Null<Sound> = step.get("sound");
			var easeFunc:Dynamic = getEase(easeName);
			var loop:Null<String> = step.get("loop");
			var remainFor:Null<Float> = step.get("remainFor");
			var waitForAction:Null<String> = step.get("waitForAction");
			var choiceTimer:Null<String> = step.get("choiceTimer");
			var waitForChoice:Null<String> = step.get("waitForChoice");
			var cueAction:Null<String> = step.get("cueAction");
			var text:Null<String> = step.get("text");
			var textX:Null<Float> = step.get("textX");
			var textY:Null<Float> = step.get("textY");
			var textBackground:Null<Texture> = step.get("textBackground");
			var textBubbleShape:Null<Texture> = step.get("textBubbleShape");
			var textTail:Null<Texture> = step.get("textTail");
			var textFont:Null<Font> = step.get("textFont");
			var fontSize:Null<Float> = step.get("fontSize");
			var color:Null<Int> = step.get("color");
			var soundLoops:Null<Int> = step.get("soundLoops");
			var tf:Null<TextBubble> = null;
			
			if (easeName != null)
			{
				if (easeName.indexOf("back", 0) > -1)
					_eases.backFuncs();
				else if (easeName.indexOf("quad",0) > -1)
					_eases.quadFuncs();
				else if (easeName.indexOf("bounce",0) > -1)
					_eases.bounceFuncs();
				else if (easeName.indexOf("circ",0) > -1)
					_eases.circFuncs();
				else if (easeName.indexOf("cube", 0) > -1)
					_eases.cubeFuncs();
				else if (easeName.indexOf("expo", 0) > -1)
					_eases.expoFuncs();
				else if (easeName.indexOf("quart", 0) > -1)
					_eases.quartFuncs();
				else if (easeName.indexOf("quint", 0) > -1)
					_eases.quintFuncs();
				else if (easeName.indexOf("sine", 0) > -1)
					_eases.sineFuncs();
			}
			if (toTime == null)
				toTime = 0;
			if (del == null)
				del = 0;
				
			 if (text != null) //---> CURRENT way of handling text bubbles.  We've got to watch for race conditions 
			{
				tf = new TextBubble(textFont, text,textBubbleShape,textTail,textBackground,preserveAspectRatio == "true" ? true : false);
				tf.pointerEnabled = false;
				trace("remain for:", remainFor);
				if (textTail != null)
					tf.orientWithTail(Std.parseFloat(step.get("textX")), Std.parseFloat(step.get("textY")),Sprite.getBounds(image.owner).x,Sprite.getBounds(image.owner).y,Sprite.getBounds(image.owner).width,Sprite.getBounds(image.owner).height);
				else 
					tf.setXY(Std.parseFloat(step.get("textX")), Std.parseFloat(step.get("textY")));
				image.owner.parent.addChild(tf.owner);
			}
			
			if (sound != null)
			{
				Timer.delay(function() { sound.play(); Timer.delay(function() { trace("disposing sound", sound); sound.dispose(); }, Std.int(sound.duration * 1000)); }, del);
			}
			
			var props:Dynamic = {};
			if (toX != null)
				props.x = toX;
			if (toY != null)
				props.y = toY;
			if (toWidth != null)
				props.width = toWidth;
			if (toHeight != null)
				props.height = toHeight;
			if (rotation != null)
				props.rotation = rotation;
			if (scaleX != null)
				props.scaleX = scaleX;
			if (nextTimeFrame != null)
				props.nextTimeFrame = nextTimeFrame;
			if (end != null)
				props.end = end;
			if (scaleY != null)
			{
				props.scaleY = scaleY; 
			}
			if (toAlpha != null)
				props.alpha = toAlpha;
				
			var paramArray:Array<Dynamic> = [];
			if (loop != null)
			{
				paramArray = [image, false, textSprites];
				_dataInWaiting = { pr:props, im:image, toT:toTime, de:del, ea:easeFunc, params:paramArray, text:tf, textRemainFor:remainFor };
				EventHub.getInstance().fire("waitForAction",waitForAction);
				EventHub.getInstance().addListener("signalContinue", this, true);
				tween(props, image, toTime, del, easeFunc, [image, false, textSprites], loop);  //TODO  refactor this so only one tween() call is needed
				if (tf != null)
				{
					if (Reflect.field(props, "x") != null)
					{
						var xDiff:Float = tf.x._ + (Reflect.field(props, "x") - image.x._);
					
						tf.x.behavior = new Sine(tf.x._, xDiff, toTime);
					}
					if (Reflect.field(props, "y") != null)
					{
						var yDiff:Float = tf.y._ + (Reflect.field(props, "y") - image.y._);
						tf.y.behavior = new Sine(tf.y._, yDiff, toTime);
					}
				}
					
			}
			else if (waitForAction != null)
			{
				paramArray = [image, false, textSprites];
				_dataInWaiting = { pr:props, im:image, toT:toTime, de:del, ea:easeFunc, params:paramArray, text:tf, textRemainFor:remainFor };
				EventHub.getInstance().fire("waitForAction",waitForAction);
				EventHub.getInstance().addListener("signalContinue", this, true);
			}
			else if (waitForChoice != null)
			{
				paramArray = [image, false, textSprites];
				_dataInWaiting = { pr:props, im:image, toT:toTime, de:del, ea:easeFunc, params:paramArray, text:tf, textRemainFor:remainFor };
				EventHub.getInstance().fire("waitForChoice", { sprite:image, choice:waitForChoice } );
				EventHub.getInstance().addListener("signalContinue", this, true);
			}
			else 
			{
				tween(props, image, toTime, del, easeFunc,[image, false, textSprites]); 
			}
			
			if (choiceTimer != null)
			{
				EventHub.getInstance().fire("choiceTimer", choiceTimer);
			}
			
			if (cueAction != null) 
			{
				EventHub.getInstance().fire("cueAction", {action:cueAction, sprite:image});
			}
		}
	}
	
	public override function dispose():Void 
	{
		_entity.dispose();
		_dataInWaiting = {};
		super.dispose();
	}
	
	private function prepareNextAnimation(image:Dynamic, rev:Bool = false, textSprites:Map<String,Dynamic> = null, nextTimeFrame:Null<String> = null, oldImage:Sprite = null, oldX:Float = -1, oldY:Float = -1):Void 
	{
		EventHub.getInstance().fire("stepDone");
		if (oldImage != null)
		{
			if (oldImage.x._ == oldX && (oldImage.x._ + (oldImage.getNaturalWidth() * oldImage.scaleX._) < 0) || oldImage.y._ == oldY && (oldImage.y._ + (oldImage.getNaturalHeight() * oldImage.scaleY._) < 0) ) 
			{ 
				oldImage.dispose(); 
			}
		}
		if (nextTimeFrame != null)
			EventHub.getInstance().fire("nextTimeFrame", _keyCode);
		animate(image, rev, textSprites);
	}
	
	private function tween(props:Dynamic, image:Sprite, time:Float, delay:Float, easeFunc = null, params:Array<Dynamic> = null, loop:Null<String> = null):Void 
	{
		if (loop == null)
		{
			Tweenimate.to(image, time, props, delay, function() {  prepareNextAnimation(params[0], params[1], params[2], props.nextTimeFrame, image, props.x, props.y); }, easeFunc);
		}
			else 
				Tweenimate.to(image, time, props, delay, function() {  tween(props, image, time, delay, easeFunc, params, loop); }, easeFunc, true);
	}
	
	private function getEase(easeName:Null<String>):Dynamic 
	{
		if (easeName == null)
			return null;
		var EaseClass = Type.resolveClass("flambe.animation.Ease");
		var allFields = Type.getClassFields(EaseClass);
		for (field in allFields)
		{
			if (easeName == field)
			{	
				return Reflect.field(EaseClass, easeName);
			}
		}
		return null;
	}
}