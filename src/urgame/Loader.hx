package urgame;

import flambe.Entity;
import flambe.System;
import flambe.asset.AssetPack;
import flambe.asset.Manifest;
import flambe.display.FillSprite;
import flambe.display.ImageSprite;
import flambe.display.TextSprite;
import flambe.display.Font;
import flambe.asset.File;
import flambe.util.Promise;
import flambe.util.SignalConnection;
import flambe.util.Signal0;
import haxe.Timer;

/**
 * ...
 * @author ...
 */
class Loader
{
	
	private var _assetPacks:Array<AssetPack>;
	private var _assetStrings:Array<String>;
	private var _background:ImageSprite;
	private var _loader:Promise<AssetPack>;
	private var _cnx:SignalConnection;
	private var _startingValue:Float;
	private var _totalAssets:Int;
	private var _loadingDone:Signal0;
	private var _manifest:Manifest;
	private var _imageName:String;
	private var _text:TextSprite;
	private var _timer:Timer;

	public function new(assetList:Array<String>, image:String = "wallpaper") 
	{
		_imageName = image;
		_loadingDone = new Signal0();
		_assetStrings = assetList;
		_assetPacks = new Array<AssetPack>();
		_totalAssets = _assetStrings.length;
		if (_manifest == null)
		{
			_manifest = Manifest.fromAssets("loading", true);
			var iterator = _manifest.iterator();
			for (iterator in _manifest)
			{
				trace(iterator.name, iterator.bytes,iterator.format,iterator.url);
			}
		}
		if (_timer == null)
		{
			_timer = new Timer(100);
			_timer.run = gpuCheck;
		}
        
		
	}
	
	private function gpuCheck()
	{
		trace (System.renderer.maxTextureSize, System.renderer.hasGPU);
		if (System.renderer.hasGPU._)
		{
			_timer.stop();
			_loader = System.loadAssetPack(_manifest);
			_loader.get(onLoadWallpaper);
		}
	}
	
	public function loadingDone():Signal0
	{
		return _loadingDone;
	}
	
	public function getAssets():Array<AssetPack>
	{
		return _assetPacks;
	}
	
	private function onLoadWallpaper(pack:AssetPack):Void 
	{
		trace("loadwallpaper");
		var wallpaper = pack.getTexture(_imageName);
		trace(wallpaper);
		_background = new ImageSprite(wallpaper);
		_text = new TextSprite(new Font(pack, "Arial"), "Loading...");
		_startingValue = -_background.getNaturalWidth();
		_background.x._ = -_startingValue;
		
		var ent:Entity = new Entity();
		ent.addChild(new Entity().add(_text));
		ent.addChild(new Entity().add(_background));
		_text.x._ = 700;
		_text.y._ = 300;
		System.root.addChild(ent);
		loadNextPack();
	}
	
	private function loadNextPack():Void 
	{
		//	var wallpaper = pack.getTexture("wallpaper");
		//_background = new ImageSprite(wallpaper);
		//_background.x._ = -_background.getNaturalWidth();
		 var manifest = Manifest.fromAssets(_assetStrings.shift());
        _loader = System.loadAssetPack(manifest);
		var multiplier:Float = 1 / _totalAssets;
		var currentStartingPoint:Float = _startingValue * multiplier * (_assetStrings.length+1);
		
		//trace(currentStartingPoint, multiplier);
		_cnx = _loader.progressChanged.connect(function() { _background.x._ =  currentStartingPoint + (_loader.progress / _loader.total) * ( -_startingValue * multiplier); 	} );
        _loader.get(batchDone);
	}
	
	private function batchDone(pack:AssetPack):Void 
	{
		_assetPacks.push(pack);
		// var manifest = Manifest.buildLocalized("bootstrap2");
        //_loader = System.loadAssetPack(manifest);
		_cnx.dispose();
		if (_assetStrings.length > 0)
			loadNextPack();
		else 
			_loadingDone.emit();
		//loader.progressChanged.connect(function() { _background.x._ =  -640 + ((loader.progress/loader.total) * 320); trace(_background.x); } );
        //loader.get(secondBatch);
	}
	
}