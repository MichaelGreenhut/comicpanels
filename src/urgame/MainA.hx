package urgame;

import flambe.Entity;
import flambe.System;
import flambe.asset.AssetPack;
import flambe.math.Rectangle;
import flambe.swf.MovieSprite;
import flambe.swf.MovieSymbol;
import flambe.swf.Format;
import flambe.swf.Library;
import flambe.animation.Ease;
import flambe.asset.Manifest;
import flambe.asset.File;
import flambe.script.Script;
import flambe.script.AnimateTo;
import flambe.script.Delay;
import flambe.script.Repeat;
import flambe.script.Sequence;
import flambe.script.Parallel;
import flambe.display.FillSprite;
import flambe.display.ImageSprite;
import haxe.remoting.DelayedConnection;

using flambe.util.Xmls;
enum BasicColor 
{
	red;
	blue;
	green;
	yellow;
	black;
	white;
}

enum Color 
{
	RGB(r:Int, g:Int, b:Int);
	RGBA(r:Int, g:Int, b:Int, a:Int);
}

class MainA
{
	
    private static function main ()
    {
        // Wind up all platform-specific stuff
        System.init();

        // Load up the compiled pack in the assets directory named "bootstrap"
        var manifest = Manifest.fromAssetsLocalized("bootstrap");
        var loader = System.loadAssetPack(manifest);
        loader.get(onSuccess);
    }

    private static function onSuccess (pack :AssetPack)
    {
        // Add a solid color background
        //var background = new FillSprite(0xefefee, System.stage.width, System.stage.height);
        //System.root.addChild(new Entity().add(background));
		//var flumpAnimator = new FlumpAnimator(pack, "flump.xml");
		
		var contents:File = pack.getFile("animations.xml");
		var animator:Animator = new Animator(pack, contents);
		
		
		var bc = BasicColor.yellow;
		//trace(bc);
		System.root.addChild(new Entity().add(animator));
		
		/*
		var library:Library = new Library(pack, "default/Flumper");
		var scene = library.createMovie("Symbol3");
		scene.scissor = new Rectangle(30, 0, 20, 100);
		System.root.addChild(new Entity().add(scene));
		*/
		
		
		
        // Add a plane that moves along the screen
       /* var plane = new ImageSprite(pack.getTexture("panel001"));
		plane.scaleX._ = 0.5;
		plane.scaleY._ = 0.5;
	
        plane.x._ = 30;
		plane.x.behavior = new Sine(30, 100, 0.5);
		//plane.x.animateTo(100,2);
		
		var entity = new Entity();
		entity.add(plane);
        System.root.addChild(entity);
		
		var script = new Script();
		var parallel = new Sequence();
		parallel.add(new AnimateTo(Reflect.field(plane, "rotation"),45,3));
		parallel.add(new AnimateTo(plane.y, 200, 3));
		script.run(new Repeat(new Sequence([parallel]),-1));
		
		entity.add(script);
		*/
		//Actuate.tween(plane, 2, { y:100 } );
		
    }
}
