-there's an odd issue with the text bubbles, where if you leave a panel on the screen with one at the end of a timeframe, then call that panel back in the next timeframe, the text bubble will be left behind. 



-rename some of these subfolders; "urgame" was a placeholder and should be "panels" for this project, "interface" for interface, etc.
-there may no longer be a need for the silly EventHub functionality; I created that when I was still dependent on Flash/OpenFL 
types, and it's probably less efficient than just using signals properly. 
-more, better garbage collection


new -> init (xml) -> begin (xml) ->
begin:
	-for every image, creates a concatenated name from the name + ";" + either the file path (for a png) or the folder path (for flump)
	-puts that concatenated name into the image queue (if it doesn't find it there already)
	creates an imageData map (string,dynamic) for all the initial image attributes and 
	steps (imageData.get("steps").push(stepData)) inside the image, and pushes it into _images.
	-pushes _images array as an entry into the _timeframes array 
	-calls loadImages();
	
loadImages:
	-parses each of the concatenated image names based on what kind of image it is (flump movie, another Animator, or Image Sprite), loads the image accordingly, then calls loadImages() again until the _imageLoadingQueue is empty.  
	Once that happens, it creates _firstSprite, adds it to the display list, and calls animate() if _autobegin was set.
	
animate: 
	-goes through each image in each timeFrame, takes the steps and attributes previously created, puts them into an animationQueue, and initializes the animationQueue. 
	
	
ANIMATION QUEUE
uses dataInWaiting for action pauses 

	